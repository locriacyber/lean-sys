# lean-sys
![crates.io](https://img.shields.io/crates/v/lean-sys)
![docs.rs](https://img.shields.io/docsrs/lean-sys)

Rust bindings to [Lean 4](https://github.com/leanprover/lean4)'s C API

Functions and comments manually translated from those in the [`lean.h` header](https://github.com/leanprover/lean4/blob/master/src/include/lean/lean.h) provided with Lean 4